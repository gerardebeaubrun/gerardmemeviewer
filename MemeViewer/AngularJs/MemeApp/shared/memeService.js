﻿/**
Service aiming at wrapping all methods around the retrieval and modification of the memes
**/
angular.module('memeApp').factory('memeService', ['$http', function ($http) {
	
	return {		

		getAvailableMemeCatalog: function () {
			return $http({
				method: 'GET',
				url: 'Home/GetAvailableMemes',
				cache: true
			})
			.success(function (data) {
				return data;
			})
			.error(function (data, status, headers, config, statusText) {
				
			});
		},

		GetAvailableCategories: function () {
			return $http({
				method: 'GET',
				url: 'Home/GetAvailableCategories',
				cache: true
			})
			.success(function (data) {
				return data;
			})
			.error(function (data, status, headers, config, statusText) {
				
			});
		}

	};

} ]);