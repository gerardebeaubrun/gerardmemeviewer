﻿
angular.module('memeApp')
.factory('galleryService', ['$http', function ($http) {

	return {
		getMeme: function (id) {
			return $http({
				method: 'GET',
				url: 'Gallery/ViewMeme/' + id,
				cache: true
			})
			.success(function (data) {
				return data;
			})
			.error(function (data, status, headers, config, statusText) {

			});
		},

		editMeme: function (id, edit) {
			return $http({
				method: 'POST',
				url: 'Gallery/EditMeme/' + id + '/' + edit.title + '/' + edit.description,
				cache: true
			})
			.success(function (data) {
				return data;
			})
			.error(function (data, status, headers, config, statusText) {

			});
		},

		deleteMeme: function (id) {
			return $http({
				method: 'POST',
				url: 'Gallery/DeleteMeme/' + id,
				cache: true
			})
			.success(function (data) {
				return data;
			})
			.error(function (data, status, headers, config, statusText) {

			});
		}

	};

} ])
.controller('galleryController', ['$scope', 'galleryService', '$routeParams', '$location', 'memeService',
	function ($scope, galleryService, $routeParams, $location, memeService) {
		//setting a property that waits for the full catalog data to come back
		$scope.dataReady = false;
		$scope.memeEdit = false;
		$scope.memeDelete = false;
		$scope.favoriteTexts = ['Add to Favorites', 'Remove From Favorites'];
		$scope.favoriteText = $scope.favoriteTexts[0];

		$scope.selectedCategory = '';

		$scope.addTofavorite = true;

		$scope.favoriteCounter = 0;
		$scope.edit = { title: '', category: '', description: '', favorite: 0 };

		//Retrieving the full meme catalago data
		galleryService.getMeme($routeParams.memeId).then(function (response) {
			$scope.memeCollection = response.data;

			$scope.dataReady = true;

			$scope.memeOrderBy = { property: 'SavedDate', order: false };
		});

		$scope.toggleDelete = function () {
			$scope.memeEdit = false;
			$scope.memeDelete = !$scope.memeDelete;
			$scope.edit = null;
		};

		$scope.toggleEdit = function () {
			$scope.memeDelete = false;
			$scope.memeEdit = !$scope.memeEdit;
		};

		$scope.toggleFavorite = function (num) {
			if ($scope.favoriteCounter === 0) {
				$scope.favoriteText = $scope.favoriteTexts[num];
				$scope.edit.favorite = (num === 1) ? 0 : 1;
				$scope.favoriteText = $scope.favoriteTexts[$scope.edit.favorite];
			}
			else {
				$scope.edit.favorite = ($scope.edit.favorite === 1) ? 0 : 1;
				$scope.favoriteText = $scope.favoriteTexts[$scope.edit.favorite];
			}
			$scope.favoriteCounter++;
		};


		$scope.deleteMeme = function () {
			galleryService.deleteMeme($routeParams.memeId);
			$scope.returnHome();
		};

		$scope.editMeme = function () {
			//$scope.edit.category = ($scope.selectedCategory);
			galleryService.editMeme($routeParams.memeId, $scope.edit);
			$location.path('#/Home/ViewMeme/' + $routeParams.memeId);
		};


		$scope.returnHome = function () {
			$location.path('#/Home/');
		};

		memeService.GetAvailableCategories().then(function (response) {
			$scope.categories = response.data;
			$scope.categories.splice(0, 0, "All");
		});

	}
]);