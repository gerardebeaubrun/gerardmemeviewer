﻿angular.module("memeApp")

.directive("recentlyAddedGallery", function () {
	return {
		restrict: "A",
		templateUrl: "AngularJs/MemeApp/home/recentGallery.html",
		controller: "homeController"
		}
})

.directive("leftSideMenu", function () {
	return {
		restrict: "A",
		templateUrl: "AngularJs/MemeApp/home/menu.html",
		link: function (scope) {
			var YTMenu = (function () {

				function init() {

					[ ].slice.call(document.querySelectorAll('.dr-menu')).forEach(function (el, i) {

						var trigger = el.querySelector('div.dr-trigger'),
				icon = trigger.querySelector('span.dr-icon-menu'),
				open = false;

						trigger.addEventListener('click', function (event) {
							if (!open) {
								el.className += ' dr-menu-open';
								open = true;
							}
						}, false);

						icon.addEventListener('click', function (event) {
							if (open) {
								event.stopPropagation();
								open = false;
								el.className = el.className.replace(/\bdr-menu-open\b/, '');
								return false;
							}
						}, false);

					});
				}

				init();

			})();
		}
	};
})
.directive("viewOneMeme", function () {
	return {
		restrict: "A",
		templateUrl: "AngularJs/MemeApp/gallery/viewMeme.html"
	}
})
.directive("editBox", function () {
	return {
		restrict: "A",
		templateUrl: "AngularJs/MemeApp/gallery/editBox.html"
	}
})
.directive("uploadImage", function () {
	return {
		restrict: "A",
		templateUrl: "AngularJs/MemeApp/gallery/uploadImage.html"
	}
})
.directive("deleteBox", function () {
	return {
		restrict: "A",
		templateUrl: "AngularJs/MemeApp/gallery/deleteBox.html"
	}
})
.directive('fileModel', ['$parse', function ($parse) {
	return {
		restrict: 'A',
		link: function (scope, element, attrs) {
			var model = $parse(attrs.fileModel);
			var modelSetter = model.assign;

			element.bind('change', function () {
				scope.$apply(function () {
					modelSetter(scope, element[0].files[0]);
				});
			});
		}
	};
} ]);