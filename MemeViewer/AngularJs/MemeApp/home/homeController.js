﻿//Contains all the directives that will render in the MemeApp

angular.module('memeApp').controller('homeController', ['$scope', 'memeService','$location',
	function ($scope, memeService, $location) {
		//setting a property that waits for the full catalog data to come back
		$scope.dataReady = false;
		$scope.selectedCategory = '';


		//Retrieving the full meme catalago data
		memeService.getAvailableMemeCatalog().then(function (response) {
			$scope.memeCollection = response.data;
			$scope.dataReady = true;

			$scope.memeOrderBy = { property: 'Category', order: false };
		});

		memeService.GetAvailableCategories().then(function (response) {
			$scope.categories = response.data;
			$scope.categories.splice(0, 0, "All");
		});

		$scope.categoryUpdate = function () {
			if ($scope.selectedCategory === "All") {
				$location.path('#/Home/');
			}
		};

		$scope.$watch('selectedCategory', function () {
			if ($scope.selectedCategory === "All") {
				$location.path('#/Home/');
			}
		});

	}
]);