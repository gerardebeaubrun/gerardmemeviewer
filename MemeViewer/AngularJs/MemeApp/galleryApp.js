﻿/**
Hub for the meme viewer app
Contains the route structure of the meme viewer app
**/

angular.module("galleryApp", ["ngRoute"]).config(function ($routeProvider) {
	$routeProvider
	.when("/Gallery/", {
		templateUrl: "AngularJs/MemeApp/gallery/gallery.html",
		controller: "galleryController"
	})
	.otherwise({
		redirectTo: "/Gallery"
	});
});