﻿/**
Hub for the meme viewer app
Contains the route structure of the meme viewer app
**/

angular.module("memeApp", ["ngRoute"]).config(function ($routeProvider) {
	$routeProvider
	.when("/Home/", {
		templateUrl: "AngularJs/MemeApp/home/home.html",
		controller: "homeController"
	})
	.when("/Home/ViewMeme/:memeId/", {
		templateUrl: "AngularJs/MemeApp/gallery/gallery.html",
		controller: "galleryController"
	})
	.when("/Home/UploadImage/", {
		templateUrl: "AngularJs/MemeApp/gallery/upload.html",
		controller: "uploadController"
	})
	.otherwise({
		redirectTo: "/Home"
	});
});