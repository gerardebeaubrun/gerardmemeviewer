﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeMeViewer.Models;
using MeMeViewer.Services;
using Newtonsoft.Json;
using MeMeViewer.Utilities;

namespace MeMeViewer.Controllers
{
    public class GalleryController : Controller
    {
       private meme_Entities memeEntities = new meme_Entities();

		/// <summary>
		/// Directs the user the first page of the app
		/// </summary>
		/// <returns> sends user the the Gallery page.
		/// angularJs will take care of the other routes/pages
		/// </returns>
		public ActionResult Index()
		{
			return View();
		}

		/// <summary>
		/// Directs the user the first page of the app
		/// </summary>
		/// <returns> sends user the the Gallery page.
		/// angularJs will take care of the other routes/pages
		/// </returns>
		public ActionResult ViewMeme(string id)
		{
		//declaring the list of memes that will be sent back to the UI
			var results = this.memeEntities.meme_Table.ToList();

			IList<Meme> listMeme = new List<Meme>();

			//Sending a List of Meme Objects instead of the meme entities
			foreach (var meme in results)
			{
				if (meme.meme_Id.ToString() == id)
				{
					listMeme.Add(new Meme
					{
						Title = meme.title,
						Description = meme.description,
						SavedDate = (DateTime)meme.savingDate,
						Address = meme.address,
						Favorite = (int)meme.favorite,
						Category = meme.category_Table.categoryName,
						Accessible = (int)meme.accessible,
						MemeId = meme.meme_Id
					});
				}
			}
			
			var memeCatalogJson = JsonConvert.SerializeObject(listMeme);

			return new ContentResult()
			{
				Content = memeCatalogJson,
				ContentEncoding = System.Text.Encoding.UTF8,
				ContentType = JsonContentType.Application.JsonResponse
			};
		}

		/// <summary>
		/// Delete the selected Meme.
		/// </summary>
		/// <returns> snull.
		/// </returns>
		public ActionResult DeleteMeme(int id)
		{
			
			//Using Linq to find the item to delete
			this.memeEntities.meme_Table.DeleteObject(
							(from x in this.memeEntities.meme_Table
							where x.meme_Id == id
							select x).First());
			this.memeEntities.SaveChanges(); 

			return null;			
		}

		/// <summary>
		/// Edit the selected Meme.
		/// </summary>
		/// <returns> snull.
		/// </returns>
		public ActionResult EditMeme(int id, string title, string description)
		{

			int endTitle = (title.Length >10) ? 10 : title.Length;
			//int endCategory = (category.Length > 10) ? 10 : category.Length;
			int endDes = (description.Length > 100) ? 10 : description.Length;

			int categoryId = 13;

			//Using linq to find the item to edit
			meme_Table tableEntry = (from x in this.memeEntities.meme_Table
							where x.meme_Id == id
							select x).First();

			//if (!string.IsNullOrEmpty(category))
			//{
			//    var catId = (from x in this.memeEntities.category_Table
			//                    where x.categoryName == category
			//                    select x.categoryId);
			//    categoryId = Convert.ToInt32(catId);
			//}


			if (tableEntry !=null)
			{
				tableEntry.category_Id = categoryId;
				tableEntry.title = (!String.IsNullOrEmpty(title)) ? title.Substring(0, endTitle) : tableEntry.title;
				tableEntry.description = (!String.IsNullOrEmpty(description)) ? description.Substring(0, endDes) : tableEntry.description;
			}

			this.memeEntities.SaveChanges();

			return null;
		}

    }
}
