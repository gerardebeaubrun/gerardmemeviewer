﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Text;
using System.IO;
using System.Web.Mvc;
using MeMeViewer.Models;


namespace MeMeViewer.Controllers
{
	public class UploadController : Controller
	{
		private meme_Entities memeEntities = new meme_Entities();


		[System.Web.Http.HttpPost]
		public System.Web.Mvc.ContentResult UploadImage(HttpPostedFileBase file)
		{
			var filename = Path.GetFileName(file.FileName);
			var path = Path.Combine(Server.MapPath("~/Images/memes/"), filename);
			file.SaveAs(path);

			int endTitle = (filename.Length > 10) ? 10 : filename.Length;

			this.memeEntities.meme_Table.AddObject(new meme_Table {
				title = filename.Substring(0, endTitle),
				address = "Images/memes/" + filename,
				savingDate = DateTime.Now,
				accessible = 1,
				category_Id = 13,
				favorite = 0
				});

			this.memeEntities.SaveChanges();


			return new System.Web.Mvc.ContentResult
			{
				ContentType = "text/plain",
				Content = filename,
				ContentEncoding = Encoding.UTF8
			};
		}
	
		
	}
}