﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeMeViewer.Services;
using Newtonsoft.Json;
using MeMeViewer.Models;
using MeMeViewer.Utilities;
using System.Net;
using System.Web.Http;
using System.Text;
using System.IO;


namespace MeMeViewer.Controllers
{
	public class HomeController : Controller
	{
		private readonly IMemeRetrieverService memeRetrieverService;
		private meme_Entities memeEntities = new meme_Entities();


		/// <summary>
		/// Initialize a new instance of the HomeController.
		/// </summary>
		/// <param name="memeRetrieverService"></param>
		public HomeController(IMemeRetrieverService memeRetrieverService=null)
		{
			this.memeRetrieverService = memeRetrieverService;
		}

		/// <summary>
		/// Initialize a new instance of the HomeController.
		/// </summary>
		public HomeController()
		{

		}

		/// <summary>
		/// This controller methods will most likely be accessed through an ajax call from the UI
		/// in order to retrieve the availaible categories needed
		/// </summary>
		/// <returns> a JSON containing the whole collection of memes available</returns>
		public ActionResult GetAvailableCategories()
		{
			//declaring the list of memes that will be sent back to the UI
			var results = this.memeEntities.category_Table.ToList();			
			
			IList<string> categoryList = new List<string>();

			foreach(var c in results )
			{
				categoryList.Add(c.categoryName);
			}

			var memeCatalogJson = JsonConvert.SerializeObject(categoryList);

			return new ContentResult()
			{
				Content = memeCatalogJson,
				ContentEncoding = System.Text.Encoding.UTF8,
				ContentType = JsonContentType.Application.JsonResponse
			};
		}

		/// <summary>
		/// Directs the user the first page of the app
		/// </summary>
		/// <returns> sends user the the home page.
		/// angularJs will take care of the other routes/pages
		/// </returns>
		public ActionResult Index()
		{
			return View();
		}
		
		/// <summary>
		/// This controller methods will most likely be accessed through an ajax call from the UI
		/// in order to retrieve the meme data needed
		/// </summary>
		/// <returns> a JSON containing the whole collection of memes available</returns>
		public ActionResult GetAvailableMemes()
		{
			//declaring the list of memes that will be sent back to the UI
			var results = this.memeEntities.meme_Table.ToList();

			IList<Meme> listMeme = new List<Meme>();

			//Sending a List of Meme Objects instead of the meme entities
			foreach (var meme in results)
			{
				listMeme.Add(new Meme {
						Title = meme.title,
						Description = meme.description,
						SavedDate = (DateTime)meme.savingDate,
						Address = meme.address,
						Favorite= (int)meme.favorite,
						Category = meme.category_Table.categoryName,
						Accessible = (int)meme.accessible,
						MemeId = meme.meme_Id						
					});
			}
			
			var memeCatalogJson = JsonConvert.SerializeObject(listMeme);

			return new ContentResult()
			{
				Content = memeCatalogJson,
				ContentEncoding = System.Text.Encoding.UTF8,
				ContentType = JsonContentType.Application.JsonResponse
			};
		}

	}
}
