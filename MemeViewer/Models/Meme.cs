﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeMeViewer.Models
{
	/// <summary>
	/// Holds the data for a meme object.
	/// </summary>
	public class Meme
	{
		/// <summary>
		/// Gets and sets the MemeID of a meme object
		/// </summary>
		public int MemeId { get; set; }
		
		/// <summary>
		/// Gets and sets the title of a meme object
		/// </summary>
		public string Title { get; set; }

		/// <summary>
		/// Gets and sets the description of a meme object
		/// </summary>
		public string Description { get; set; }

		/// <summary>
		/// Gets and sets the Category of a meme object
		/// </summary>
		public string Category { get; set; }

		/// <summary>
		/// Gets and sets the address of a meme object
		/// </summary>
		public string Address { get; set; }

		/// <summary>
		/// Gets and sets the favorite of a meme object
		/// </summary>
		public int Favorite { get; set; }

		/// <summary>
		/// Gets and sets the Accessible property of a meme object
		/// </summary>
		public int Accessible { get; set; }

		/// <summary>
		/// Gts and sets the date when the item was saved of a meme object
		/// </summary>
		public DateTime SavedDate { get; set; }

		
	}
}
