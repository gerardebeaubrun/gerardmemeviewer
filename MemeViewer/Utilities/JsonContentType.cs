﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MeMeViewer.Utilities
{
	public class JsonContentType
	{
		/// <summary>
		/// Content type header constant.
		/// </summary>
		public const string CharSetToAppendForContentTypeHeader = "; charset=UTF-8";

		/// <summary>
		/// This class is an enhanced version of the System.Net.Mime.MediaTypeNames.Application class.
		/// </summary>
		public static class Application
		{
			// Specified by System.Net.Mime.MediaTypeNames.Application:

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Octet = "application/octet-stream";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Pdf = "application/pdf";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Rtf = "application/rtf";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Soap = "application/soap+xml";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Zip = "application/zip";

			// New specifications:

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string GZip = "application/x-gzip";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string JsonRequest = "application/json";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string JsonResponse = "application/json";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Doc = "application/msword";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Ppt = "application/vnd.ms-powerpoint";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Pptx = "application/vnd.openxmlformats-officedocument.presentationml.presentation";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Xls = "application/vnd.ms-excel";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string Xlsx = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

			// West specifications:

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			public const string WestGroupSmartLabel = "application/x-wgsl";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			[Obsolete("Until all major browsers support this, use HttpContentTypeNames.Text.Html ('text/html') instead.", true)]
			public const string Xhtml = "application/xhtml+xml";

			/// <summary>
			/// Constant for the Octet Mime Media type.
			/// </summary>
			[Obsolete("Until all major browsers support this, use HttpContentTypeNames.Text.JavaScript ('text/javascript') instead.", true)]
			public const string JavaScript = "application/javascript";
		}

		/// <summary>
		/// This class is an enhanced version of the System.Net.Mime.MediaTypeNames.Image class.
		/// </summary>
		public static class Image
		{
			// Specified by System.Net.Mime.MediaTypeNames.Image:

			/// <summary>
			/// Constant for the mime type image/gif.
			/// </summary>
			public const string Gif = "image/gif";

			/// <summary>
			/// Constant for the mime type image/gif.
			/// </summary>
			public const string Jpeg = "image/jpeg";

			/// <summary>
			/// Constant for the mime type image/gif.
			/// </summary>
			public const string Tiff = "image/tiff";

			// New specifications:

			/// <summary>
			/// Constant for the mime type image/gif.
			/// </summary>
			[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Png", Justification = "csymons - This is a known file extension.")]
			public const string Png = "image/png";

			/// <summary>
			/// Constant for the mime type image/gif.
			/// </summary>
			public const string Bitmap = "image/bmp";

			/// <summary>
			/// Constant for the mime type image/gif.
			/// </summary>
			public const string Icon = "image/x-icon";
		}

		/// <summary>
		/// This class is an enhanced version of the System.Net.Mime.MediaTypeNames.Text class.
		/// </summary>
		public static class Text
		{
			// Specified by System.Net.Mime.MediaTypeNames.Text:

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Html = "text/html";

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Plain = "text/plain";

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string RichText = "text/richtext";

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Xml = "text/xml";

			// New specifications:

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string JavaScript = "text/javascript";

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Css = "text/css";

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Calendar = "text/calendar";

			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Vcard = "text/x-vcard";
		}

		/// <summary>
		/// This class is a completely new part of the System.Net.Mime.MediaTypeNames class which has not been represented thus far.
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1034:NestedTypesShouldNotBeVisible", Justification = "csymons - Used this in Cobalt for WestlawNext.")]
		public static class Multipart
		{
			/// <summary>
			/// Constant for the mime type text/gif.
			/// </summary>
			public const string Mixed = "multipart/mixed; boundary=\"----=_boundary\"";
		}
	}
}