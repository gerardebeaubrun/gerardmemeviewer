﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeMeViewer.Models;

namespace MeMeViewer.Services
{
	/// <summary>
	/// Handles any business logic for the retrieval of memes.
	/// </summary>
	public interface  IMemeRetrieverService
    {
		/// <summary>
		/// retrieves all the memes stored by the user
		/// </summary>
		/// <returns></returns>
		IList<meme_Table> RetrieveMemeCollection();
    }
}
