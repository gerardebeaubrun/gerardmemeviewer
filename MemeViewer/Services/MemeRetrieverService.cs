﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MeMeViewer.Models;
using System.Data;
using System.Data.SqlClient;

namespace MeMeViewer.Services
{
	public class MemeRetrieverService : IMemeRetrieverService
	{
		private meme_Entities memeEntities = new meme_Entities();
		
		public IList<meme_Table> RetrieveMemeCollection()
		{
			return this.memeEntities.meme_Table.ToList();			
		}

	}

}
