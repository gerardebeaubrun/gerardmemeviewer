﻿using System.Web;
using System.Web.Optimization;

namespace MeMeViewer
{
	public class BundleConfig
	{
		// For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(						
						"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
						"~/Scripts/jquery.fileupload.js", 
						"~/Scripts/jquery-ui.js",
						"~/Scripts/jquery-ui.min.js",
						"~/Scripts/jquery-ui-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
						"~/Scripts/jquery.unobtrusive*",
						"~/Scripts/jquery.validate*"));

			// Use the development version of Modernizr to develop with and learn from. Then, when you're
			// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
						"~/Scripts/modernizr-*"));

			bundles.Add(new StyleBundle("~/Content/css").Include(
						"~/Content/site.css",
						//AngularJS Styling						
						"~/AngularJs/Css/menu/component.css", 
						"~/AngularJs/Css/menu/default.css",
						"~/AngularJs/Css/gallery/style.css",
						"~/AngularJs/Css/gallery/reset.css",
						"~/AngularJs/Css/gallery/gallery.css",
						"~/AngularJs/Css/gallery/styleGallery.css",
						"~/AngularJs/Css/gallery/upload.css",
						"~/AngularJs/Css/form/form.css",
						"~/AngularJs/Css/twitterbootStrap/css/bootstrap-*",
						"~/AngularJs/Css/twitterbootStrap/css/bootstrap.css",
						"~/AngularJs/Css/twitterbootStrap/css/bootstrap.min.css",
						"~/Scripts/jquery-ui.css",
						"~/Scripts/jquery-ui.min.css",
						"~/Scripts/jquery-ui.structure.css",
						"~/Scripts/jquery-ui.structure.min.css",
						"~/Scripts/jquery-ui.theme.css",
						"~/Scripts/jquery-ui.theme.min.css"));
			

			bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
						"~/Content/themes/base/jquery.ui.core.css",
						"~/Content/themes/base/jquery.ui.resizable.css",
						"~/Content/themes/base/jquery.ui.selectable.css",
						"~/Content/themes/base/jquery.ui.accordion.css",
						"~/Content/themes/base/jquery.ui.autocomplete.css",
						"~/Content/themes/base/jquery.ui.button.css",
						"~/Content/themes/base/jquery.ui.dialog.css",
						"~/Content/themes/base/jquery.ui.slider.css",
						"~/Content/themes/base/jquery.ui.tabs.css",
						"~/Content/themes/base/jquery.ui.datepicker.css",
						"~/Content/themes/base/jquery.ui.progressbar.css",
						"~/Content/themes/base/jquery.ui.theme.css"					
						));


			//AngularJS Bundles
			bundles.Add(new ScriptBundle("~/bundles/angularSetUp").Include(
						"~/AngularJs/lodash.js",
						"~/AngularJs/angular-loader.js",
						"~/AngularJs/angular-route.js",
						"~/AngularJs/angular.js"));

			bundles.Add(new ScriptBundle("~/bundles/angularMemeApp").Include(
						"~/AngularJs/MemeApp/memeApp.js",
						"~/AngularJs/MemeApp/javascript/init.js",
						"~/AngularJs/MemeApp/javascript/portfolio.js",
						"~/AngularJs/MemeApp/galleryApp.js",
						"~/AngularJs/MemeApp/gallery/galleryController.js",
						"~/AngularJs/MemeApp/gallery/uploadController.js",	
						"~/AngularJs/MemeApp/home/homeDirectives.js",
						"~/AngularJs/MemeApp/shared/memeService.js",
						"~/AngularJs/MemeApp/home/homeController.js")); 
						

			
		}
	}
}