﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MeMeViewer
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(name: "Default", url: "{controller}/{action}", defaults: new { controller = "Home", action = "Index" });
			routes.MapRoute(name: "MemeViewer.RetrieveMemeCatalog", url: "{controller}/{action}", defaults: new { controler = "Home", action = "GetAvailableMemes" });
			routes.MapRoute(name: "MemeViewer.RetrieveMemeCategories", url: "{controller}/{action}", defaults: new { controler = "Home", action = "GetAvailableCategories" });
			routes.MapRoute(name: "MemeViewer.Gallery", url: "{controller}/{action}", defaults: new { controller = "Gallery", action = "Index" });
			routes.MapRoute(name: "MemeViewer.ViewMeme", url: "{controller}/{action}/{id}", defaults: new { controller = "Gallery", action = "ViewMeme" });
			routes.MapRoute(name: "MemeViewer.DeleteMeme",  url: "{controller}/{action}/{id}", defaults: new { controller = "Gallery", action = "DeleteMeme" });
			routes.MapRoute(name: "MemeViewer.EditMeme",    url: "{controller}/{action}/{id}/{title}/{description}", defaults: new { controller = "Gallery", action = "EditMeme" });
			routes.MapRoute(name: "MemeViewer.UploadImage", url: "{controller}/{action}", defaults: new { controler = "Upload", action = "UploadImage" });

		}
	}
}